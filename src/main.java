import java.util.Scanner;
public class main {

    public static void main (String[] args) {
        Scanner input = new Scanner(System.in);
        Tictactoe game = new Tictactoe();
        game.initializeBoard();
        String player = "X";
        do {
            System.out.println(game.printBoard());
            System.out.println("Enter coordinates for row: ");
            int row = input.nextInt();
            System.out.println("Enter coordinates for column: ");
            int col = input.nextInt();
            game.setPlay(row, col, player);
            if(game.isGameOver()) {
                System.out.println(game.printBoard() + "\n" + player + " wins!");
                break;
            }
            if(player == "X")
                player = "0";
            else
                player = "X";
        }while(true);
    }
}
